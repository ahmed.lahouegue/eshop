import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent} from './home/home.component';
import { AddProductComponent} from './add-product/add-product.component';
import {CommonModule} from "@angular/common";

const ROUTES: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'addProduct', component: AddProductComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(ROUTES), CommonModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
