import { Component, OnInit } from '@angular/core';
import {Product} from "../model/product";

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {
  product: Product;
  list: Product[]= [];
  productList: Product[];
  constructor() { }

  ngOnInit(): void {
    this.product = new Product();

  }
  addProduct(product:Product){


    this.list.push(product);
  }
}
