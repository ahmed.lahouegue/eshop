import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormProductComponent } from './form-product/form-product.component';
import {FormsModule} from "@angular/forms";
import { HomeComponent } from './home/home.component';
import { AddProductComponent } from './add-product/add-product.component';
import {ProductsComponent} from "./products/products.component";

@NgModule({
  declarations: [
    AppComponent,
    FormProductComponent,
    HomeComponent,
    AddProductComponent,
    ProductsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
