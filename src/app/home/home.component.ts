import { Component, OnInit } from '@angular/core';
import {Product} from "../model/product";
import {global} from "@angular/compiler/src/util";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  productList: Product[];
  constructor() { }

  ngOnInit(): void {
    this.productList= [
      {id: 1, title: "T-shirt 1", price: 18, quantity: 0, like: 0},
      {id: 2, title: "T-shirt 2", price: 21, quantity: 10, like: 0},
      {id: 3, title: "T-shirt 3", price: 16, quantity: 8, like: 0}];
  }
  buyProduct(i: number){
    this.productList[i].quantity--;
  }

}
